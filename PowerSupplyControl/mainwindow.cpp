#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initVariables();
    checkPorts();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initVariables()
{
    spManager = new SerialPortManager();
    spmThread = new QThread();
    spManager->moveToThread(spmThread);
    spmThread->start();

    bytesRead = 0;
    bytesWritten = 0;

    connect(this, &MainWindow::readyStart, spManager, &SerialPortManager::initVariables);
    connect(spManager, &SerialPortManager::dataReceivedU, ui->readedVoltageLbl, &QLabel::setText);
    connect(spManager, &SerialPortManager::dataReceivedU, this, &MainWindow::updateRxLbl);
    connect(spManager, &SerialPortManager::dataReceivedI, ui->readedCurrentLbl, &QLabel::setText);
    connect(spManager, &SerialPortManager::dataReceivedI, this, &MainWindow::updateRxLbl);
    connect(ui->setVoltageBtn, &QPushButton::clicked, this, &MainWindow::setVoltage);
    connect(this, &MainWindow::voltageSetted, spManager, &SerialPortManager::sendVoltage);
    connect(ui->setCurrentBtn, &QPushButton::clicked, this, &MainWindow::setCurrent);
    connect(this, &MainWindow::currentSetted, spManager, &SerialPortManager::sendCurrent);
    connect(spManager, &SerialPortManager::dataSent, this, &MainWindow::updateTxLbl);
    connect(ui->comboBoxComPorts, &QComboBox::textActivated, spManager, &SerialPortManager::setComPort);

    emit readyStart();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox *warningDialog = new QMessageBox();
    warningDialog->setWindowTitle("Выход из системы");
    warningDialog->setIcon(QMessageBox::Question);
    warningDialog->addButton("Да",QMessageBox::AcceptRole);
    warningDialog->addButton("Нет",QMessageBox::RejectRole);

    warningDialog->setInformativeText("");
    warningDialog->setText("Вы действительно хотите выйти из системы?");

    int i = warningDialog->exec();
    if (i==0)
    {
        event->accept();
    }
    else
    {
        event->ignore();
    }
}

void MainWindow::setVoltage()
{
    double voltage = ui->setVoltageDoubleSpinBox->value();

    emit voltageSetted(voltage);
}

void MainWindow::setCurrent()
{
    double current = ui->setCurrentDoubleSpinBox->value();

    emit currentSetted(current);
}

void MainWindow::updateTxLbl(qint64 bytesWritten)
{
    if(bytesWritten > 0)
    {
        this->bytesWritten += bytesWritten;
        QString strBytesWritten;
        strBytesWritten = QString::number(this->bytesWritten);
        ui->txLbl->setText(strBytesWritten);
    }
}

void MainWindow::updateRxLbl(QByteArray data)
{
    bytesRead += data.size();
    QString strBytesRead;
    strBytesRead = QString::number(bytesRead);
    ui->rxLbl->setText(strBytesRead);
}

void MainWindow::checkPorts()
{
    ui->comboBoxComPorts->clear();

    foreach(const QSerialPortInfo& info, QSerialPortInfo::availablePorts())
    {
        ui->comboBoxComPorts->addItem(info.portName());
    }
}
