#include "lcd.h"

void LCD_Init()
{
	e0;
	HAL_Delay(50);
	rs0;
	
	
	LCD_WriteData(0x2);
	HAL_Delay(1);
	e1;
	HAL_Delay(1);
	e0;
	HAL_Delay(1);
	
	
	LCD_WriteData(0x2);
	HAL_Delay(1);
	e1;
	HAL_Delay(1);
	e0;
	HAL_Delay(1);
	
	
	LCD_WriteData(0xF);
	HAL_Delay(1);
	e1;
	HAL_Delay(1);
	e0;
	HAL_Delay(1);
	
	
  LCD_WriteData(0x0);
	HAL_Delay(1);
	e1;
	HAL_Delay(1);
	e0;
	HAL_Delay(1);
	
	
	LCD_WriteData(0xF);
	HAL_Delay(1);
	e1;
	HAL_Delay(1);
	e0;
	HAL_Delay(1);
	
	
	LCD_Command(0x01);
	
	LCD_Command(0x02);
}

// ks0066u datasheet
//void LCD_Init()
//{
//	e0;
//	HAL_Delay(50);
//	rs0;
//	
//	/* Function set */
//	
//	LCD_WriteData(0x2);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	LCD_WriteData(0x2);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	LCD_WriteData(0x12);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	/* ------------ */
//	
//	/* Display ON/OFF Control */
//	
//	LCD_WriteData(0x0);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	LCD_WriteData(0x12);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	/* ---------------------- */
//	
//	/* Display Clear */
//	
//  LCD_WriteData(0x0);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	LCD_WriteData(0x1);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	/* ------------- */
//	
//	/* Entry Mode Set */
//	
//	LCD_WriteData(0x0);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	LCD_WriteData(0x7);
//	HAL_Delay(1);
//	e1;
//	HAL_Delay(1);
//	e0;
//	HAL_Delay(1);
//	
//	/* -------------- */
//}

void LCD_WriteData(uint8_t dt)
{
	if(((dt >> 3)&0x01)==1) {d7_set();} else {d7_reset();}
	if(((dt >> 2)&0x01)==1) {d6_set();} else {d6_reset();}
	if(((dt >> 1)&0x01)==1) {d5_set();} else {d5_reset();}
	if((dt&0x01)==1) {d4_set();} else {d4_reset();}
}

void LCD_Command(uint8_t dt)
{
	rs0;
	//HAL_Delay(1);
	LCD_WriteData(dt>>4);
	//HAL_Delay(1);
	e1;
	//HAL_Delay(1);
	e0;
	//HAL_Delay(1);
	LCD_WriteData(dt);
	//HAL_Delay(1);
	e1;
	//HAL_Delay(1);
	e0;
	HAL_Delay(1);
}

void LCD_Data(uint8_t dt)
{
	rs1;
	//HAL_Delay(1);
	LCD_WriteData(dt>>4);
	//HAL_Delay(1);
	e1;
	//HAL_Delay(1);
	e0;
	//HAL_Delay(1);
	LCD_WriteData(dt);
	//HAL_Delay(1);
	e1;
	//HAL_Delay(1);
	e0;
	HAL_Delay(1);
}

void LCD_ClearDisplay(void)
{
	LCD_Command(0x01);
}

void LCD_ReturnHome(void)
{
	LCD_Command(0x02);
}

void LCD_EntryModeSet(void)
{
	LCD_Command(0x06);
}

void LCD_DisplayOnOff(void)
{
	LCD_Command(0x0C);
}

void LCD_FunctionSet(void)
{
	LCD_Command(0x28);
}

void LCD_SendChar(char ch)
{
	LCD_Data((uint8_t)ch);
}

void LCD_String(char* str)
{
	for(uint8_t i = 0; str[i] != '\0'; i++)
	{
		LCD_Data((uint8_t)str[i]);
	}
}

void LCD_SetPos(uint8_t x, uint8_t y)
{
	switch(y){
		
		case 0:
			LCD_Command(x|0x80);
      break;
		
    case 1:
      LCD_Command((0x40 + x)|0x80);
      break;
	}
}