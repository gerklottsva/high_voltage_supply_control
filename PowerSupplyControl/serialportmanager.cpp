#include "serialportmanager.h"

SerialPortManager::SerialPortManager(QObject *parent) : QObject(parent)
{

}

void SerialPortManager::initVariables()
{
    serial  = new QSerialPort(this);
    timer = new QTimer();
    timer->start(1000);

    connect(serial, &QSerialPort::readyRead, this, &SerialPortManager::serialReceive);
    connect(timer, &QTimer::timeout, this, &SerialPortManager::sendConnectionStatusByte);

    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        QSerialPort port;
        port.setPort(info);
        if(port.open(QIODevice::ReadWrite))
        {
            qDebug() << "Название: " + info.portName() + " " + info.description() + info.manufacturer();
        }
    }
}

void SerialPortManager::destroyVariables()
{
    serial->close();
    delete serial;
}

void SerialPortManager::serialReceive()
{
    QByteArray ba;
    ba = serial->readAll();

    if(ba.contains('u'))
    {
        ba.chop(1);
        if(ba.size() == 7)
            emit dataReceivedU(ba);
    }
    else if(ba.contains('i'))
    {
        ba.chop(1);
        if(ba.size() == 7)
            emit dataReceivedI(ba);
    }
}

void SerialPortManager::sendVoltage(double voltage)
{
    serial->setDataBits(QSerialPort::DataBits::UnknownDataBits);

    QString strVoltage = QString::number(voltage, 'g', 3);
    strVoltage.append('u');
    qint64 bytesWritten;
    bytesWritten = serial->write(strVoltage.toLocal8Bit());
    serial->waitForBytesWritten();

    emit dataSent(bytesWritten);
}

void SerialPortManager::sendCurrent(double current)
{
    serial->setDataBits(QSerialPort::DataBits::UnknownDataBits);

    QString strCurrent = QString::number(current, 'g', 3);
    strCurrent.append('i');
    qint64 bytesWritten;
    bytesWritten = serial->write(strCurrent.toLocal8Bit());
    serial->waitForBytesWritten();

    emit dataSent(bytesWritten);
}

void SerialPortManager::setComPort(const QString& portName)
{
    if(serial->isOpen())
        serial->close();

    serial->setPortName(portName);
    serial->setBaudRate(QSerialPort::Baud38400);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    serial->open(QIODevice::ReadWrite);
}

void SerialPortManager::sendConnectionStatusByte()
{
    serial->setDataBits(QSerialPort::DataBits::UnknownDataBits);

    QString strStatusConnection = "s";
    qint64 bytesWritten;
    bytesWritten = serial->write(strStatusConnection.toLocal8Bit());
    serial->waitForBytesWritten();

    emit dataSent(bytesWritten);
}
