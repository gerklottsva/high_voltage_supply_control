#ifndef SERIALPORTMANAGER_H
#define SERIALPORTMANAGER_H

#include <QObject>
#include <QSerialPort>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QTimer>

#include <QDebug>

class SerialPortManager : public QObject
{
    Q_OBJECT
public:
    explicit SerialPortManager(QObject *parent = nullptr);

    void initVariables();
    void destroyVariables();

signals:
    void dataReceivedU(QByteArray data);
    void dataReceivedI(QByteArray data);

public slots:
    void serialReceive();
    void sendVoltage(double voltage);
    void sendCurrent(double current);
    void setComPort(const QString& portName);

private slots:
    void sendConnectionStatusByte();

signals:
    void dataSent(qint64 bytesWritten);

private:
    QSerialPort *serial;
    QTimer* timer;
};

#endif // SERIALPORTMANAGER_H
