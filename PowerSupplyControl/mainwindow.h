#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>
#include <QThread>
#include "serialportmanager.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void setVoltage();
    void setCurrent();
    void updateTxLbl(qint64 bytesWritten);
    void updateRxLbl(QByteArray data);
    void checkPorts();

private:
    Ui::MainWindow *ui;
    QSerialPort *serial;

    SerialPortManager* spManager;
    QThread* spmThread;

    quint64 bytesRead;
    quint64 bytesWritten;

    void initVariables();
    void closeEvent(QCloseEvent* event);

signals:
    void readyStart();
    void voltageSetted(double voltage);
    void currentSetted(double current);

};
#endif // MAINWINDOW_H
