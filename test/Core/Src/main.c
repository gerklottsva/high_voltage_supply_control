/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* Переменные для работы с ADC и DAC */
float uOut = 0;																						/*!< Выходное напряжение */
float iOut = 0;																						/*!< Выходной ток */
float uHand = 0;																					/*!< Управляющее напряжение с потенциометра */
uint16_t uOutDac = 0;																			/*!< Цифровое значениие выходного напряжения для настройки ЦАПа */
/*-----------------------------------*/

/* Переменные для работы с UART */
#define SIZE_TX_BUFFER_U 8																/*!< Размер буфера отправляемого по UART для выходного напряжения */
#define SIZE_TX_BUFFER_I 8																/*!< Размер буфера отправляемого по UART для выходного тока */
#define MAX_LENGTH_OF_RECEIVED_DATA 5											/*!< Размер буфера принимаемого по UART */
uint8_t tx_buffer_u[SIZE_TX_BUFFER_U];										/*!< Буффер для отправки по UART выходного напряжения */
uint8_t tx_buffer_i[SIZE_TX_BUFFER_I];										/*!< Буффер для отправки по UART выходного тока */
uint8_t rx_buffer;																				/*!< Буфер для приёма данных по UART */
uint8_t received_data[MAX_LENGTH_OF_RECEIVED_DATA];				/*!< Буфер для полной посылки данных, принятой по UART */
volatile uint8_t rx_counter = 0;													/*!< Счётчик полученных данных по UART */
float u_from_uart = 0;																		/*!< Значение выходного напряжения для установки, полученное по UART */
float i_from_uart = 0;																		/*!< Значение выходного тока для установки, полученное по UART */
uint8_t statusConnectionCounter = 2;											/*!< Счётчик для контроля соединения по UART */
uint32_t timer2Counter = 0;																/*!< Счётчик таймера 2 */
uint32_t timer3Counter = 0;																/*!< Счётчик таймера 3 */
uint32_t lcdOutputControl = 0;														/*!< Переменная для контроля вывода информации на дисплей */
uint32_t uartOutputControl = 0;														/*!< Переменная для контроля вывода информации на дисплей */
/*------------------------------*/

/* Переменные для устранения дребезга контактов при нажатии кнопки */
uint8_t btnTurnedOn = 0;
uint32_t timeBtnTurnedOff = 0;
/*-----------------------------------------------------------------*/

/* Переменные для отображения на дисплее */
uint8_t uOutLcd[12];
uint8_t iOutLcd[12];
/*---------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void checkButton(void);
void setDac(void);
void displayUAndIOnLcd(void);
void sendByUart(void);

/* Прототипы колбэк функций для работы с АЦП */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc);
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc);
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC2_Init();
  MX_DAC_Init();
  MX_USART1_UART_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start_IT(&htim1);
	HAL_TIM_Base_Start_IT(&htim2);
	HAL_TIM_Base_Start_IT(&htim3);
	HAL_UART_Receive_IT(&huart1, &rx_buffer, 1);
	HAL_DAC_Start(&hdac,DAC_CHANNEL_2);
	MEAS_ADC_start();
	LCD_Init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		checkButton();
		
		getUout(&uOut);
		getIout(&iOut);
		getUhand(&uHand);
		
		setDac();	
		
		sendByUart();
		
		displayUAndIOnLcd();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_TIM1
                              |RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV4;
  PeriphClkInit.Tim1ClockSelection = RCC_TIM1CLK_HCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) 
{
   if(hadc)
	 {
		 MEAS_ADC1_eval(1);
	 }
}

void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc) 
{
   if(hadc)
	 {
		 MEAS_ADC1_eval(0);
	 }
}

/**
  * @brief Колбэк функция для работы с таймером
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM1)
	{
		if(statusConnectionCounter < 255)
		{
			statusConnectionCounter++;
		}
	}
	if(htim->Instance == TIM2)
	{
		timer2Counter++;
	}
	if(htim->Instance == TIM3)
	{
		timer3Counter++;
	}
}

/**
  * @brief Колбэк функция для работы с UART
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) 
{
	received_data[rx_counter] = rx_buffer;
	rx_counter++;
	
	if (rx_buffer == 'u') 
	{
		sscanf((char*)received_data, "%f", &u_from_uart);
		
		for(int i = 0; i < MAX_LENGTH_OF_RECEIVED_DATA; i++)
		{
			received_data[i] = 0;
		}
		rx_counter = 0;
	}
	else if (rx_buffer == 'i') 
	{
		sscanf((char*)received_data, "%f", &i_from_uart);
		
		for(int i = 0; i < MAX_LENGTH_OF_RECEIVED_DATA; i++)
		{
			received_data[i] = 0;
		}
		rx_counter = 0;
	}
	else if (rx_buffer == 's') 
	{
		statusConnectionCounter = 0;
		
		for(int i = 0; i < MAX_LENGTH_OF_RECEIVED_DATA; i++)
		{
			received_data[i] = 0;
		}
		rx_counter = 0;
	}
	
	HAL_UART_Receive_IT(&huart1, &rx_buffer, 1);
}

/**
	* @brief Функция для установки значения ЦАПа
  */
void setDac(void)
{
	if(btnTurnedOn == 0)
	{
		if(statusConnectionCounter < 2)
		{
			if(iOut > (i_from_uart / 1000) && u_from_uart > 0)
			{
				u_from_uart = u_from_uart - 0.05;
			}
			
			uOutDac = (u_from_uart / 1.666 - 0.0666) / 3.3 * 4095;
			HAL_DAC_SetValue(&hdac,DAC_CHANNEL_2,DAC_ALIGN_12B_R, uOutDac);
		}
		else
		{
			u_from_uart = 0;
			uOutDac = 0;
			HAL_DAC_SetValue(&hdac,DAC_CHANNEL_2,DAC_ALIGN_12B_R, uOutDac);
		}
	}
	else
	{
		u_from_uart = 0;
		uOutDac = uHand / 3.3 * 4095;
			
		HAL_DAC_SetValue(&hdac,DAC_CHANNEL_2,DAC_ALIGN_12B_R, uOutDac);
	}
}

/**
  * @brief Функция для проверки нажатости кнопки
  */
void checkButton(void)
{
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_10) == GPIO_PIN_SET)	// Если кнопка в выключенном состоянии
	{
		timeBtnTurnedOff = HAL_GetTick();	// Считываем время этого состояния
	}

	if((HAL_GetTick() - timeBtnTurnedOff) > 300 && (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_10) == GPIO_PIN_RESET))	// Если текущее время минус время последнего выключенного состояния больше 300 мс и кнопка во включенном состоянии
	{
		btnTurnedOn = 1;
	}
	else
	{
		btnTurnedOn = 0;
	}

	if(btnTurnedOn)
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_RESET);
	}
}

/**
  * @brief Функция отображения на дисплее выходных напряжения и тока
  */
void displayUAndIOnLcd(void)
{
	if(lcdOutputControl != timer2Counter)
	{
		sprintf((char*)uOutLcd, "Uout=%.2fkV", uOut);
		sprintf((char*)iOutLcd, "Iout=%.2fmA", iOut);
		LCD_SetPos(2, 0);
		LCD_String((char*)uOutLcd);
		LCD_SetPos(2, 1);
		LCD_String((char*)iOutLcd);
		
		lcdOutputControl = timer2Counter;
	}
}

/**
  * @brief Функция отправки по UART выходных напряжения и тока
  */
void sendByUart(void)
{
	if(uartOutputControl != timer3Counter)
	{
		sprintf((char*)tx_buffer_u, "%.2fkV", uOut);
		sprintf((char*)tx_buffer_i, "%.2fmA", iOut);
		tx_buffer_u[7] = 'u';
		tx_buffer_i[7] = 'i';
		//HAL_UART_Transmit_IT(&huart1, tx_buffer_u, sizeof(tx_buffer_u), 1000);
		HAL_UART_Transmit(&huart1, tx_buffer_u, sizeof(tx_buffer_u), 50);
		HAL_Delay(1);
		//HAL_UART_Transmit_IT(&huart1, tx_buffer_i, sizeof(tx_buffer_i), 1000);
		HAL_UART_Transmit(&huart1, tx_buffer_i, sizeof(tx_buffer_i), 50);
		
		uartOutputControl = timer3Counter;
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
