#ifndef INC_MEAS_H_
#define INC_MEAS_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"

#define MEAS_ADC1_CHANNELS 4			// ADC channels

uint8_t MEAS_ADC_start(void);

void MEAS_ADC_stop(void);
void MEAS_ADC1_eval(uint8_t pos);
void getUout(float* u);
void getIout(float* i);
void getUhand(float* u);
void getTemp(float *t);

#ifdef __cplusplus
}
#endif

#endif /* INC_MEAS_H_ */
